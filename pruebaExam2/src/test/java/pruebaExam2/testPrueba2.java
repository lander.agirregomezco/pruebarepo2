package pruebaExam2;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value= Parameterized.class)
public class testPrueba2 {

	int n;
	//boolean result;
	
	@Parameters
	public static Collection<Object []> parameters(){
		return Arrays.asList(new Object[] [] {
//			{1,false},
//			{2,true},
//			{3,true},
//			{4,false},
//			{5,true}
			{0},
			{1},
			{4},
			{6}
		}
		);
	}
//	public Parameter_test(int n, boolean expect) {
//		this.n=n;
//		this.result=expect;
//		
//	}
//
	public testPrueba2(int n) {
		this.n=n;
		
	}
	
	@Test
	public void test() {
		pruebaExam2 exam=new pruebaExam2();
		//asserEquals(result,exam.exam(n));
		
		assertFalse(exam.exam(n));
		
		
	}

}
